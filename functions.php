<?php

/* Include all files in folders (/inc/*). */
require get_stylesheet_directory() . '/inc/init.php';
require get_stylesheet_directory() . '/package-control/init.php';

function dbg($var, $die = false){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    if( $die )
        die();
    else
        return;
}
function redirect_non_admin_user(){
    if ( !defined( 'DOING_AJAX' ) && !current_user_can('administrator') ){
        wp_redirect( site_url() );  exit;
    }
}
add_action( 'admin_init', 'redirect_non_admin_user' );
function target_main_category_query_with_conditional_tags( $query ) {
    if ( ! is_admin() && $query->is_main_query() ) {
        // Not a query for an admin page.
        // It's the main query for a front end page of your site.
        if ( is_category() ) {
            $query->set( 'post_type', 'any' );
        }
    }
}
add_action( 'pre_get_posts', 'target_main_category_query_with_conditional_tags' );
add_action( 'add_meta_boxes', 'cd_meta_box_add' );
function cd_meta_box_add()
{
    $post_types = ['games'];
    add_meta_box( 'game-meta-box-id', 'Game Meta', 'cd_meta_box_cb', $post_types, 'normal', 'high' );
//    add_meta_box( 'game-meta-box-id', 'Call to action link', 'cd_meta_box_cb', post, 'normal', 'high' );
}
function cd_meta_box_cb($post)
{
    $values = get_post_meta( $post->ID );
    $game_type = isset( $values['metabox_game_type'] ) ? esc_attr( $values['metabox_game_type'][0] ) : "";
    $game_rating = isset( $values['metabox_game_rating'] ) ? esc_attr( $values['metabox_game_rating'][0] ) : "";
    wp_nonce_field( 'game_meta_box_nonce', 'meta_box_nonce' );
    ?>
    <div class="onnet-meta text-meta">
        <div class="onnet-meta-label">
            <label for="metabox_game_type">Game Type</label>
            <p>
            </p>
        </div>
        <div class="onnet-meta-content">
            <input type="text" name="metabox_game_type" id="metabox_game_type" value="<?php echo $game_type ?>"></div></div>
    <div class="onnet-meta text-meta">
        <div class="onnet-meta-label">
            <label for="metabox_game_rating">Game game_Rating</label>
            <p>
            </p>
        </div>
        <div class="onnet-meta-content">
            <input type="text" name="metabox_game_rating" id="metabox_game_rating" value="<?php echo $game_rating ?>"></div></div>
    <?php
}
add_action( 'save_post', 'cd_meta_box_save' );
function cd_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'game_meta_box_nonce' ) ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
    // now we can actually save the data
    $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchors can only have href attribute
        )
    );
    // Make sure your data is set before trying to save it
    if( isset( $_POST['metabox_game_type'] ) )
        update_post_meta( $post_id, 'metabox_game_type', wp_kses( $_POST['metabox_game_type'], $allowed ) );
    if( isset( $_POST['metabox_game_rating'] ) )
        update_post_meta( $post_id, 'metabox_game_rating', wp_kses( $_POST['metabox_game_rating'], $allowed ) );
}

function child_theme_setup() {
    load_child_theme_textdomain( 'product.onportal', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'child_theme_setup' );