<?php
/**
 * Widget Single Template: Wallpaper Single
 */

// header
if (isset($data->header) && $data->header): ?>
    <div class="infoBar--variant1" data-component>
        <div class="infoBar--variant1__wrapper">
            <div class="infoBar--variant1__textWrapper">
                <h4><?php the_title(); ?></h4>
            </div>
        </div>
    </div>
<?php endif;

$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'medium');
$image = (isset($image) && $image[0] != '') ? $image[0] : '';

// Wallpapers
$heading = get_post_meta(get_the_ID(), 'metabox_download_heading', true);
$small_title = get_post_meta(get_the_ID(), 'metabox_download_small_wallpaper_title', true);
$small_link = get_post_meta(get_the_ID(), 'metabox_download_small_wallpaper', true);
$medium_title = get_post_meta(get_the_ID(), 'metabox_download_medium_wallpaper_title', true);
$medium_link = get_post_meta(get_the_ID(), 'metabox_download_medium_wallpaper', true);
$large_title = get_post_meta(get_the_ID(), 'metabox_download_large_wallpaper_title', true);
$large_link = get_post_meta(get_the_ID(), 'metabox_download_large_wallpaper', true);

?>

    <!-- Component: article - variant1 -->
    <article class="article--variant1" itemscope itemtype="http://schema.org/Article">
            <?php if (has_post_thumbnail()): ?>
                <span class="article--variant1__featuredImgWrapper">
					<?php new OnNet_image('featured'); ?>
				</span>
            <?php endif; ?>

        <?php // Show social sharing
        if (isset($data->social) && $data->social):
            if (function_exists("wp_nav_menu")) :
                wp_nav_menu(array(
                        'menu_class' => 'socialShare--variant1__list',
                        'sort_column' => 'menu_order',
                        'theme_location' => 'social_share',
                        'container' => 'div',
                        'container_class' => 'socialShare--variant1',
                        'walker' => new Social_share,
                        'fallback_cb' => false,
                    )
                );
            endif;
        endif; ?>

        <?php if (isset($data->the_content) && $data->the_content): ?>
            <div class="article--variant1__body">
                <?php the_content(); ?>
            </div>
        <?php endif; ?>

        <?php if (isset($data->wallpapers) && $data->wallpapers == '1' || isset($data->article_links) && $data->article_links != 'no_links') : ?>

            <?php if (!empty($small_title) && !empty($small_link) || !empty($medium_title) && !empty($medium_link) || !empty($large_title) && !empty($large_link) || isset($data->article_links) && $data->article_links != 'no_links') : ?>

                <footer class="article--variant1__footer">

                    <?php if (isset($data->article_links) && $data->article_links != 'no_links') { ?>

                        <a target="_blank" href="<?php echo $service_text; ?>"><?php echo $service_text; ?></a>

                    <?php } else if ((isset($data->wallpapers) && $data->wallpapers == '1')) { ?>

                        <?php if (!empty($small_title) && !empty($small_link) || !empty($medium_title) && !empty($medium_link) || !empty($large_title) && !empty($large_link)) : ?>



                            <?php if (!empty($small_title) && !empty($small_link)) : ?>
                                <a target="_blank"
                                   href="/download-image/?postid=<?php echo get_the_ID(); ?>&size=metabox_download_small_wallpaper"><?php echo $small_title; ?></a>
                            <?php endif; ?>

                            <?php if (!empty($medium_title) && !empty($medium_link)) : ?>
                                <a target="_blank"
                                   href="/download-image/?postid=<?php echo get_the_ID(); ?>&size=metabox_download_medium_wallpaper"><?php echo $medium_title; ?></a>
                            <?php endif; ?>

                            <?php if (!empty($large_title) && !empty($large_link)) : ?>
                                <a target="_blank"
                                   href="/download-image/?postid=<?php echo get_the_ID(); ?>&size=metabox_download_large_wallpaper"><?php echo $large_title; ?></a>
                            <?php endif; ?>

                        <?php endif; ?>

                    <?php } ?>

                </footer>

            <?php endif; ?>

        <?php endif; ?>

    </article>
    <!-- End of article -->