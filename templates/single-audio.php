<?php
/**
 * Widget Single Template: Audio Single
 */

// header
if (isset($data->header) && $data->header): ?>
    <div class="infoBar--variant1" data-component>
        <div class="infoBar--variant1__wrapper">
            <div class="infoBar--variant1__textWrapper">
                <h4><?php the_title(); ?></h4>
            </div>
        </div>
    </div>
<?php endif;

$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'medium');
$image = (isset($image) && $image[0] != '') ? $image[0] : '';

// Wallpapers
$heading = get_post_meta(get_the_ID(), 'metabox_download_heading', true);
$small_title = get_post_meta(get_the_ID(), 'metabox_download_small_wallpaper_title', true);
$small_link = get_post_meta(get_the_ID(), 'metabox_download_small_wallpaper', true);
$medium_title = get_post_meta(get_the_ID(), 'metabox_download_medium_wallpaper_title', true);
$medium_link = get_post_meta(get_the_ID(), 'metabox_download_medium_wallpaper', true);
$large_title = get_post_meta(get_the_ID(), 'metabox_download_large_wallpaper_title', true);
$large_link = get_post_meta(get_the_ID(), 'metabox_download_large_wallpaper', true);

// Good quality
$video = get_post_meta(get_the_ID(), 'metabox_mp4_file_url', true);
$video = (isset($video) && $video != '') ? $video : '';
$size = get_post_meta(get_the_ID(), 'metabox_mp4_file_size', true);

// Poor quality
$p_video = get_post_meta(get_the_ID(), 'metabox_3gp_file_url', true);
$p_video = (isset($video) && $video != '') ? $video : '';
$p_size = get_post_meta(get_the_ID(), 'metabox_3gp_file_size', true);

//Audio
$audio = get_post_meta(get_the_ID(), 'metabox_mp3_file_url', true);
$audio = (isset($audio) && $audio != '') ? $audio : '';
$audio_size = get_post_meta(get_the_ID(), 'metabox_mp3_file_size', true);

?>

    <article class="article--variant1" itemscope="" itemtype="http://schema.org/Article">

        <div class="media--variant2">

            <?php if (!is_feature_phone()) : ?>

                <video class="media--variant2__innerWrapper" controls poster="<?php echo $image; ?>">

                    <source src="<?php echo $audio; ?>" type="audio/mpeg">
                    Your browser does not support the <code>audio</code> element.

                </video>

            <?php else : ?>

                <div class="media--variant2__innerWrapper">
                    <img src="<?php echo $image; ?>" alt="audio">
                    <ul class="media--variant2__list">
                        <li class="media--variant2__item">
                            <a target="_blank"
                               href="<?php echo $audio; ?>">Download <?php echo $audio_size; ?></a>
                        </li>
                    </ul>
                </div>

            <?php endif; ?>

        </div>

        <?php // Show social sharing
        if (isset($data->social) && $data->social):
            if (function_exists("wp_nav_menu")) :
                wp_nav_menu(array(
                        'menu_class' => 'socialShare--variant1__list',
                        'sort_column' => 'menu_order',
                        'theme_location' => 'social_share',
                        'container' => 'div',
                        'container_class' => 'socialShare--variant1',
                        'walker' => new Social_share,
                        'fallback_cb' => false,
                    )
                );
            endif;
        endif; ?>

        <?php if (isset($data->the_content) && $data->the_content): ?>
            <div class="article--variant1__body">
                <?php the_content(); ?>
            </div>
        <?php endif; ?>

        <?php if (isset($data->wallpapers) && $data->wallpapers == '1' || isset($data->article_links) && $data->article_links != 'no_links') : ?>

            <?php if (!empty($small_title) && !empty($small_link) || !empty($medium_title) && !empty($medium_link) || !empty($large_title) && !empty($large_link) || isset($data->article_links) && $data->article_links != 'no_links') : ?>

                <footer class="article--variant1__footer">

                    <?php if (isset($data->article_links) && $data->article_links != 'no_links') { ?>

                        <a target="_blank" href="<?php echo $service_text; ?>"><?php echo $service_text; ?></a>

                    <?php } else if ((isset($data->wallpapers) && $data->wallpapers == '1')) { ?>

                        <?php if (!empty($small_title) && !empty($small_link) || !empty($medium_title) && !empty($medium_link) || !empty($large_title) && !empty($large_link)) : ?>

                            <?php if (isset($heading)) { ?>
                                <h2><?php echo $heading; ?></h2>
                            <?php }?>

                            <?php if (!empty($small_title) && !empty($small_link)) : ?>
                                <a target="_blank"
                                   href="/download-image/?postid=<?php echo get_the_ID(); ?>&size=metabox_download_small_wallpaper"><?php echo $small_title; ?></a>
                            <?php endif; ?>

                            <?php if (!empty($medium_title) && !empty($medium_link)) : ?>
                                <a target="_blank"
                                   href="/download-image/?postid=<?php echo get_the_ID(); ?>&size=metabox_download_medium_wallpaper"><?php echo $medium_title; ?></a>
                            <?php endif; ?>

                            <?php if (!empty($large_title) && !empty($large_link)) : ?>
                                <a target="_blank"
                                   href="/download-image/?postid=<?php echo get_the_ID(); ?>&size=metabox_download_large_wallpaper"><?php echo $large_title; ?></a>
                            <?php endif; ?>

                        <?php endif; ?>

                    <?php } ?>

                </footer>

            <?php endif; ?>

        <?php endif; ?>

    </article>