<?php
/**
 *  Template Name: Play Game
 */

get_portal_header('game');

if (array_key_exists('game', $_GET))
    $game = $_GET['game'];

// The Game
if (isset($game)):
    ?>

    <article class="article" itemscope itemtype="http://schema.org/Article">
        <div class="article__body">

            <?php get_game_play($game); ?>

        </div>
    </article>

    <?php

else:
    // NO GAME.
    ?>
    <article class="article" itemscope itemtype="http://schema.org/Article">
        <div class="article__body">
            <div class="iframeOuterWrapper">
                <?php
                echo 'Sorry, there has been an error loading the game...'
                ?>
            </div>
        </div>
    </article>
    <?php
endif;

get_portal_footer('game');