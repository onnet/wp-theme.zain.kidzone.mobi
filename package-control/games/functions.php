<?php
/*===================================================*/
/* - INCLUDE ALL FILES FROM INCLUDES (/inc/*) FOLDER
/*===================================================*/
// CONSTANT -> Package Theme Path
define('PACKAGEPATH', get_package_dir());
// CONSTANT -> Package Template Part
define('PACKAGEPART', package_template_dir());

function get_package_dir()
{
    return plugin_dir_path(__FILE__);
}


function package_template_dir($suffix = '')
{
    $package_control = basename(dirname(get_package_dir())) . DIRECTORY_SEPARATOR;
    $package_directory = basename(get_package_dir()) . DIRECTORY_SEPARATOR;
    $template_directory = 'templates' . DIRECTORY_SEPARATOR;

    if ($suffix !== '')
        $suffix .= DIRECTORY_SEPARATOR;

    return $package_control . $package_directory . $template_directory . $suffix;
}


foreach (glob(dirname(__FILE__) . "/inc/*.php") as $filename) {
    include($filename);
}


add_action('wp_enqueue_scripts', 'game_script');
function game_script()
{
    wp_enqueue_script('games', get_stylesheet_directory_uri() . '/package-control/games/assets/js/games.js', array('jquery', 'modernizr'), '1.0.0', true);
}

function add_taxonomies_to_pages()
{
    register_taxonomy_for_object_type('category', 'page');
}

add_action('init', 'add_taxonomies_to_pages');

function billing_active()
{
    $settings = apply_filters('api_credentials', true);

    if( !is_array($settings))
        return false;

    if( !array_key_exists('billing_active', $settings))
        return false;

    return true;
}

function get_club_description($path = 'play')
{
    $billing_active = billing_active();

    if(is_user_logged_in() && $billing_active)
        return false;

    $post = get_page_by_path($path);

    $post_id =  $post->ID;

    // Get The Club ID.
    $club_id = get_post_meta($post_id, 'hyve_content_lock', true);

    // If the club is empty or false return
    if (empty($club_id) || !$club_id)
        return false;

    // Get The Club
    $club = apply_filters('get_club_by', 'club_id', $club_id);

    // Club is not array or has no meta key.
    if (!is_array($club) || !array_key_exists('meta', $club))
        return false;

    // Set the meta
    $meta = json_decode($club['meta'] );

    // If the meta is not an object or if description is not a property
    if( !is_object($meta) || !property_exists($meta, 'description'))
        return false;

    // if the descroption is empty
    if (empty( $meta->description))
        return false;

    // return description
    return $meta->description;
}