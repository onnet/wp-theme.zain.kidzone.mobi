(function ($) {

    $(window).on("orientationchange", function () {
        resize_game();
    });

    $(document).ready(function () {
        resize_game();
    });

   /* $(window).resize(function(){
        resize_game();
    }); */

    function resize_game() {

        if ($('.onnet_games').length > 0) {
            var id = $('.iframeOuterWrapper').attr('id');
            var height = $(window).height();
            var width = $('.onnet_games').width();

            if (id === 'landscape') {
                var max_height = resize_landscape(width);
            }
            else {
                var max_height = height - 5; // resize_portrait(width);
            }

            $('.onnet_games').css({'padding-top':'5px', 'width':width, 'height': height, 'max-height': max_height});
        }
    }

    function resize_landscape(width) {
        return ((width / 4) * 3)+1;
    }

    function resize_portrait(width) {
        return ((width / 3) * 4)+1;
    }
})(jQuery);