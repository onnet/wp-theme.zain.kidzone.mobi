<?php //1870 2210
/**
 *    CPT: GAMES
 */
add_action('init', 'games_post_type');
function games_post_type()
{
    if (class_exists('Custom_Post_Type')) {
        $games = new Custom_Post_Type('games',
            array(
                'has_archive' => true,
                'supports'    => array(
                    'title',
                    'thumbnail',
                    'editor',
                    'custom-fields',
                ),
            ),
            array(),
            false
        );

        $games->add_taxonomy('Category');

        $_games = games_list();

        $games->add_meta_box(
            'game',
            array(
                'game' => array(
                    'name'        => 'game',
                    'label'       => '<b>Insert Game</b>',
                    'description' => '',
                    'input'       => 'select',
                    'default'     => 'high',
                    'options'     => $_games,
                ),
            )
        );
    }
}

function get_game_play($game)
{
    // CTL ARCADE
    if (strpos($game, 'ctl') !== false) {

        play_ctl_arcade($game);
    } else {

        play_hyve_games($game);
    }
}

function play_ctl_arcade($game)
{
    echo '<div class="iframeOuterWrapper">';
    if (shortcode_exists('ctl_arcade_game')) {
        echo do_shortcode('[ctl_arcade_game game="' . $game . '" mode="iframe" max-height="1000"]');
    } else {
        echo 'Sorry, there has been an error loading the game...';
    }
    echo '</div>';
}

function play_hyve_games($game)
{
    $hyveGames = new HyveGetGames();
    $game = $hyveGames->get_hyve_game($game);

    if (empty($game)) {

        echo '<div class="iframeOuterWrapper">Sorry, there has been an error loading the game...</div>';

        return;
    }
    
    if ($game->iframe == 0) {

        wp_redirect($hyveGames->get_game_url($game));

        return;
    }

    echo '<div class="iframeOuterWrapper" id="' . $game->aspect . '">';
    echo "<iframe class='onnet_games' width='100%' src ='".  $hyveGames->get_game_url($game) ."'></iframe>";
    echo '</div>';
    return;
}

function games_list()
{
    global $wpdb;
    $_games = array();

// CTL Arcade

    $results = $wpdb->get_results("SELECT game_name FROM {$wpdb->prefix}ctl_arcade_games");
    foreach ($results as $game) {

        $_games[ctl_game_name($game->game_name)] = $game->game_name;
    }

// Hyve Games

    $results = $wpdb->get_results("SELECT name, slug FROM {$wpdb->prefix}hyve_games");
    foreach ($results as $game) {

        $_games[$game->slug] = $game->name;
    }

    asort($_games);

    return array('0' => 'Select a game...') + $_games;
}

function ctl_game_name($name)
{
    return 'ctl-' . str_replace(' ', '-', strtolower($name));
}

/**
 *    Games Post Type Save.
 *
 *    On post save if the game has not been added to the provided meta field then
 *    the post status will be forced into draft mode.
 */
add_action('save_post', 'games_post_type_save');
function games_post_type_save()
{
    if (array_key_exists('post_type', $_POST) && $_POST['post_type'] === 'games') {
        if (is_admin() && function_exists('ctl_arcade_menu')) {
            if ($_POST['custom_meta']['game_game'] == '0') {
// Remove Hook to avoid infinite loop.
                remove_action('save_post', 'games_post_type_save');

// Set Post to Draft status.
                wp_update_post(array('ID' => $_POST['ID'], 'post_status' => 'draft'));

// Re-Hook the games_post_type_save function
                add_action('save_post', 'games_post_type_save');
            }
        }
    }

    return $_POST;
}

/**
 *    Pre-get posts: Categories
 */
add_action('pre_get_posts', 'game_categories');
function game_categories($query)
{
    if (is_category() && $query->is_main_query()) {
        $query->set('post_type', 'games');
    }

    return;
}

function get_scoreboard($game)
{
    global $wpdb;

    $filter = "SELECT * FROM {$wpdb->prefix}ctl_arcade_scores WHERE game_plugin_dir = '{$game}' AND user_id != 1 ORDER BY user_score";

    $results = $wpdb->get_results($filter);

    if (empty($results)) {
        return false;
    }

    $scoreboard = array('0');

    foreach ($results as $row) {
        $entry = new stdClass;
        $entry->name = get_username($row->user_id);
        $entry->date = get_playdate($row->time);
        $entry->score = $row->user_score;
        $scoreboard[] = $entry;
    }

    unset($scoreboard[0]);

    return $scoreboard;
}

function get_playdate($time)
{
    $date = explode(' ', $time);

    $date = $date[0];

    if ($date === date('Y-m-d')) {
        return 'Today';
    }

    if ($date === date('Y-m-d', strtotime('-1 day'))) {
        return 'Yesterday';
    }

    return $date;
}

function get_username($user_id)
{
    $user = get_userdata($row->user_id);

    if ($user->data->user_login === $user->data->user_nicename) {
        return 'Annonymous';
    } else {
        return $user->data->user_nicename;
    }
}

function get_game_aspect($game)
{
    global $wpdb;

    $sql = "SELECT game_aspect FROM  {$wpdb->prefix}onnet_games WHERE game_plugin_dir = '{$game}'";
    $result = (array) $wpdb->get_row($sql);

    if (empty($result)) {
        return '';
    }

    $aspect = $result['game_aspect'];

    if ($aspect === null) {
        return '';
    }

    return "id='{$aspect}'";
}