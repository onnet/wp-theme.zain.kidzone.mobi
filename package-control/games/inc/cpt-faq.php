<?php
/**
*   CPT: Frequently Asked Questions
*   Only Supports Title & Editor
*/
add_action('init', 'faq_post');
function faq_post()
{
    if (class_exists('Custom_Post_Type'))
    {
        $faq = new Custom_Post_Type( 'FAQ',
            array(
                'has_archive' => true,
                'supports' => array(
                    'title',
                    'editor',
                )
            )
        );
    }
}


/**
*   FAQ Archive Count
*   Remove the posts per page limit for the faq archive page.
*/
add_action('pre_get_posts', 'faq_archive_count');
function faq_archive_count( $query )
{
    if ( is_post_type_archive( 'faq' ) && $query->is_main_query() ) 
    {
        $query->set( 'posts_per_page', -1 );
        return;
    }

}