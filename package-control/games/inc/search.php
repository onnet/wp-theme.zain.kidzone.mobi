<?php
/**
 *	Search_pre_get_posts
 */
function search_pre_get_posts( $query ) {
	if ( $query->is_search() && $query->is_main_query() ) {
		// Set Search String
		$string = $_GET['s'];

		// Search for categories.
		$search = search_tax_by('name', $string);

		// Limit results to Posts only
		$query->set('post_type', array('games'));
			if($search)
            {
                $taxquery = array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'id',
                        'terms' => array( $search ),
                    )
                );
                // Set Category
                $query->set('tax_query', $taxquery);
                // Remove s filter.
                $query->set('s', '');
            } else{
                // Else SET String Filter
                $query->set( 's',  $string );
            }

	}
}
add_action( 'pre_get_posts', 'search_pre_get_posts' );


function search_tax_by($field = 'name', $string, $category='category' )
{
	$terms = get_term_by( $field, $string, $category );
	if( $terms )
		return $terms->term_id;
	else
		return $terms;

}