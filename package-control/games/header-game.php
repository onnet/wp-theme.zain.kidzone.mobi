<?php do_action('hyve_content_lock', 'games', true, 152 ); ?>
<!doctype html>
<html lang="en">

<head>

    <?php wp_head(); ?>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->

    <title><?php echo get_bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0" />
    <meta name="apple-mobile-web-app-title" content="<?php echo get_bloginfo('name'); ?>">

    <!-- Favicon Component -->
    <link rel="author" href="/humans.txt"/>
    <link rel="apple-touch-icon" sizes="57x57"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/android-chrome-192x192.png"
          sizes="192x192">
    <link rel="icon" type="image/png"
          href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage"
          content="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">


</head>

<body class="preload">

    <div class="siteContainer" id="siteContainer">

        <div class="siteContainer__content" id="siteContainerInner" style="max-width:100%">
            <main role="main">