<?php
/**
 * Widget Custom Template: Not Logged In.
 */
?>
<div class="contentList--variant1">

    <?php
        echo "<h2>" .get_the_title() . "</h2>";
    ?>

<ul class="contentList--variant1__list">
    <li class="contentList--variant1__item">
        <a class="contentList--variant1__innerWrapper" href="/play">
            <div class="contentList--variant1__textWrapper">
                <?php
                    echo '<h3>' . get_club_description('play') .'<h3>';
                ?>
            </div>
        </a>
    </li>
</ul>

</div>