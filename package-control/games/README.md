# Package Games

## Description
This package is used as a submodule in a child theme. 
Note that this package and the child theme should depend on [OnPortal](https://bitbucket.org/onnet/product.onportal).

## Installation

### Composer

Instruct composer to install this package into your child theme's package-control folder using a custom installation path that should be defined in your composer.json file.

```json
"installer-paths": {
  "{wordpress-install-dir}/wp-content/themes/{child-theme}/package-control/{$name}/": ["wp-onportal-package/games"]
}
```

Require the package

```bash
$ composer require wp-onportal-package/games
```

### Git Submodule

```bash
$ git submodule add git@bitbucket.org:onnet/package-games.git <local relative path>
```

## Configuration

Create a Package Control init file.

`/child-theme/package-control/init.php`

And insert the following code.

```php
foreach (glob(get_stylesheet_directory()."/package-control/*/functions.php") as $filename) {
	include $filename;
}
``` 

Require the Package Control init file in your child theme functions.php file

```php
require get_stylesheet_directory() . '/package-control/init.php';
```

###Template Hierarchy

Each default WordPress template has a hierarchical override or fallback. Each template can be overridden by child theme or package.

`Child` i.e. If the child theme has a `single.php` it will be used.

`Package` i.e. If there is no child theme `single.php` the package `single.php` will be used.

`Parent` i.e. If there is no child theme or package `single.php` the parent theme `single.php` will be used.

####Change Log

__1.1.0__

Added the ability to install package via composer.

__1.0.0__

* First release
* Template hierarchy added
* Package submodule created