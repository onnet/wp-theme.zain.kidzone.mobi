<?php
/**
 * Created by PhpStorm.
 * User: Mish
 * Date: 2019/07/02
 * Time: 20:04
 */


_e('Menu');
_e('menu');
_e('MENU');

_e('Home');
_e('home');
_e('HOME');

_e('R3 / day');
_e('R3 / Day');
_e('R3 / DAY');

_e('Subscribe');
_e('subscribe');
_e('SUBSCRIBE');

_e('Type your search here…');
_e('type your search here…');
_e('TYPE YOUR SEARCH HERE…');

_e('Type search here...');
_e('type search here...');
_e('TYPE SEARCH HERE...');

_e('Go');
_e('go');
_e('GO');

_e('View more');
_e('view more');
_e('VIEW MORE');

_e('Quick links');
_e('Quick Links');
_e('quick links');
_e('QUICK LINKS');

_e('Terms & conditions');
_e('terms & conditions');
_e('TERMS & CONDITIONS');

_e('Want to unsubscribe?');
_e('want to unsubscribe?');
_e('WANT TO UNSUBSCRIBE?');

_e('Price per day');
_e('price per day');
_e('PRICE PER DAY');

_e('Subscribe to ....');
_e('subscribe to ....');
_e('SUBSCRIBE TO ....');

_e('Already a member? Login');
_e('already a member? login');
_e('ALREADY A MEMBER? LOGIN');

_e('Forgot link');
_e('forgot link');
_e('FORGOT LINK');

_e('Forgot your link?');
_e('forgot your link?');
_e('FORGOT YOUR LINK?');

_e('Enter your number below');
_e('enter your number below');
_e('ENTER YOUR NUMBER BELOW');

_e('Add mobile number');
_e('add mobile number');
_e('ADD MOBILE NUMBER');

_e('Add mobile number.');
_e('add mobile number.');
_e('ADD MOBILE NUMBER.');

_e('Sms my access link');
_e('sms my access link');
_e('SMS MY ACCESS LINK');

_e('Sorry! You haven‘t subscribed');
_e('sorry! you haven‘t subscribed');
_e('SORRY! YOU HAVEN‘T SUBSCRIBED');

_e('You have to activate your profile or login to view this content.');
_e('you have to activate your profile or login to view this content.');
_e('YOU HAVE TO ACTIVATE YOUR PROFILE OR LOGIN TO VIEW THIS CONTENT.');

_e('No subscription for the number found.');
_e('no subscription for the number found.');
_e('NO SUBSCRIPTION FOR THE NUMBER FOUND.');

_e('You should receive an sms soon with your access link.');
_e('you should receive an sms soon with your access link.');
_e('YOU SHOULD RECEIVE AN SMS SOON WITH YOUR ACCESS LINK.');

_e('Download now!');
_e('download now!');
_e('DOWNLOAD NOW!');

_e('Size');
_e('size');
_e('SIZE');

_e('*Limited handset compatibility');
_e('*limited handset compatibility');
_e('*LIMITED HANDSET COMPATIBILITY');

_e('Enter your phone number below to unsubscribe.');
_e('enter your phone number below to unsubscribe.');
_e('ENTER YOUR PHONE NUMBER BELOW TO UNSUBSCRIBE.');

_e('You need to be logged in to unsubscribe. Enter your phone number below to get a login sms link sent to your phone.');
_e('you need to be logged in to unsubscribe. enter your phone number below to get a login sms link sent to your phone.');
_e('YOU NEED TO BE LOGGED IN TO UNSUBSCRIBE. ENTER YOUR PHONE NUMBER BELOW TO GET A LOGIN SMS LINK SENT TO YOUR PHONE.');

_e('Send');
_e('send');
_e('SEND');

_e('Cancel');
_e('cancel');
_e('CANCEL');

_e('Please click ‘yes’ below if you would like to unsubscribe.');
_e('please click ‘yes’ below if you would like to unsubscribe.');
_e('PLEASE CLICK ‘YES‘ BELOW IF YOU WOULD LIKE TO UNSUBSCRIBE.');

_e('Yes');
_e('yes');
_e('YES');

_e('Good bye!');
_e('good bye!');
_e('GOOD BYE!');

_e('You’ve been unsubscribed');
_e('you’ve been unsubscribed');
_e('YOU’VE BEEN UNSUBSCRIBED');

_e('You will still have access to the games for the next 24 hours.');
_e('you will still have access to the games for the next 24 hours.');
_e('YOU WILL STILL HAVE ACCESS TO THE GAMES FOR THE NEXT 24 HOURS.');

_e('Changed your mind? You can subscribe again by clicking the link below.');
_e('changed your mind? you can subscribe again by clicking the link below.');
_e('CHANGED YOUR MIND? YOU CAN SUBSCRIBE AGAIN BY CLICKING THE LINK BELOW.');

_e('To unsubscribe SMS');
_e('to unsubscribe sms');
_e('TO UNSUBSCRIBE SMS');

_e('An SMS explaining how to unsubscribe has been sent to your number.');
_e('an sms explaining how to unsubscribe has been sent to your number.');
_e('AN SMS EXPLAINING HOW TO UNSUBSCRIBE HAS BEEN SENT TO YOUR NUMBER.');

_e('Oops! Looks like something went wrong');
_e('oops! looks like something went wrong');
_e('OOPS! LOOKS LIKE SOMETHING WENT WRONG');

_e('Error! Invalid MSISDN.');
_e('error! invalid msisdn.');
_e('ERROR! INVALID MSISDN.');

_e('We were unable to complete your unsubscription from this service.');
_e('we were unable to complete your unsubscription from this service.');
_e('WE WERE UNABLE TO COMPLETE YOUR UNSUBSCRIPTION FROM THIS SERVICE.');

_e('Try Again');
_e('try again');
_e('TRY AGAIN');

_e('Back to home');
_e('back to home');
_e('BACK TO HOME');

_e('Thank you for subscribing');
_e('thank you for subscribing');
_e('THANK YOU FOR SUBSCRIBING');

_e('You will receive a confirmation SMS shortly. Please click the provided link in the SMS to complete your subscription.');
_e('you will receive a confirmation sms shortly. please click the provided link in the sms to complete your subscription.');
_e('YOU WILL REVEIVE A CONFIRMATION SMS SHORTLY. PLEASE CLICK THE PROVIDED LINK IN THE SMS TO COMPLETE YOUR SUBSCRIPTION.');

_e('Ok');
_e('ok');
_e('OK');

_e('Search results for....');
_e('search results for....');
_e('SEARCH RESULTS FOR....');

_e('No results found...');
_e('no results found...');
_e('NO RESULTS FOUND...');

_e('Next');
_e('next');
_e('NEXT');

_e('Whoops');
_e('whoops');
_e('WHOOPS');

_e('Let‘s try this again. Please select ‘try again‘ below');
_e('let‘s try this again. please select ‘try again‘ below');
_e('LET‘S TRY THIS AGAIN. PLEASE SELECT ‘TRY AGAIN‘ BELOW');

_e('Lets try this again. Please select try again below');
_e('lets try this again. please select try again below');
_e('LETS TRY THIS AGAIN. PLEASE SELECT TRY AGAIN BELOW');

_e('Not yet a member?');
_e('not yet a member?');
_e('NOT YET A MEMBER?');

_e('Join NOW!');
_e('join now!');
_e('JOIN NOW!');

_e('Not yet a member? JOIN NOW!');
_e('not yet a member? join now!');
_e('NOT YET A MEMBER? JOIN NOW!');

_e('DAY');
_e('day');
_e('Day');

_e('Copyright. All Rights Reserved');
_e('copyright. all rights reserved');
_e('COPYRIGHT. ALL RIGHTS RESERVED');

_e('Fairytales');
_e('fairytales');
_e('FAIRYTALES');

_e('Fun Games');
_e('fun games');
_e('FUN GAMES');

_e('Van Dogh');
_e('van dogh');
_e('VAN DOGH');

_e('Zumbers');
_e('zumbers');
_e('ZUMBERS');

_e('Glumpers');
_e('glumpers');
_e('GLUMPERS');

_e('Boom & Reds');
_e('boom & reds');
_e('BOOM & REDS');

_e('Alphabet');
_e('alphabet');
_e('ALPHABET');

_e('The Tales of Aunt Owl');
_e('the tales of aunt owl');
_e('THE TALES OF AUNT OWL');

_e('Laugh and Learn');
_e('laugh and learn');
_e('LAUGH AND LEARN');

_e('Kids TV Shows');
_e('kids tv shows');
_e('KIDS TV SHOWS');

_e('PLAY');
_e('play');
_e('Play');

_e('Play as often as you like – unlimited fun!');
_e('play as often as you like – unlimited fun!');
_e('PLAY AS OFTEN AS YOU LIKE - UNLIMITED FUN!');

_e('Kid Zone');
_e('kid zone');
_e('KID ZONE');

_e('Copyright. Terms and Conditions Apply.');
_e('copyright. terms and conditions apply.');
_e('COPYRIGHT. TERMS AND CONDITIONS APPLY.');

_e('LOGIN');
_e('Login');
_e('login');

_e('Subscribe NOW!');
_e('subscribe now!');
_e('SUBSCRIBE NOW!');

_e('Not subscribed yet?');
_e('not subscribed yet?');
_e('NOT SUBSCRIBED YET?');

_e('Continue');
_e('continue');
_e('CONTINUE');

_e('Enter Your Number');
_e('enter your number');
_e('ENTER YOUR NUMBER');

_e('R3.00/day subscription service');
_e('R3.00/DAY SUBSCRIPTION SERVICE');

_e('Prev');
_e('prev');
_e('PREV');

_e('Previous');
_e('previous');
_e('PREVIOUS');

_e('Back');
_e('back');
_e('BACK');

_e('SMS My Access Link');
_e('sms my access link');
_e('SMS MY ACCESS LINK');




































