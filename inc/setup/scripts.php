<?php
/**
 *   Load Scripts
 *   Load the stylesheets & the script files for the theme.
 *   If is feature phone then remove all scripts.
 */


add_action('wp_enqueue_scripts', 'load_child_scripts');
function load_child_scripts()
{
    $prefix = get_stylesheet_directory_uri() . '/assets/';
    $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG === true  ? '.' : '.min.';

    /* Styles */
    wp_enqueue_style('frontend', $prefix . 'css/frontend' . $suffix . 'css', '1.0.0', true);

    /* Scripts */
    if (!is_feature_phone())
    {
        wp_enqueue_style('font', 'http://fonts.googleapis.com/css?family=Lato:400italic,400,700', '1.0.0', true);

        wp_enqueue_script('jquery');
        wp_enqueue_script('modernizr', $prefix .  'js/modernizr' . '.min.js', 'jquery', '1.0.0',true);
        wp_enqueue_script('frontend', $prefix . 'js/frontend'  . '.js', array('jquery', 'modernizr'), '1.0.0', true);

    } else {

        // Remove scripts from feature phones
        global $wp_scripts;

        $leave_alone = array(
            // Put the scripts you don't want to remove in here.
        );

        foreach ( $wp_scripts->queue as $handle )
        {
            // Here we skip/leave-alone those, that we added above ↑
            if ( in_array( $handle, $leave_alone ) )
                continue;

            $wp_scripts->remove( $handle );
        }

    }
}